
from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken import views

urlpatterns = [
    #path('snippets/', include(snippets.urls, namespace='snippets')),
    path('api-auth/', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
    path('api/', include('mainapp.api.urls')),
    path('api-token-auth/', views.obtain_auth_token, name='api-token-auth')
 
]
